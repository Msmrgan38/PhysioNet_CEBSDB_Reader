HEADERS += \
    $$PWD/include/wfdb/ecgcodes.h \
    $$PWD/include/wfdb/ecgmap.h \
    $$PWD/include/wfdb/wfdb.h \
    $$PWD/include/wfdb/wfdblib.h

LIBS += -L/$$PWD/lib/ -lwfdb

INCLUDEPATH += $$PWD/include/wfdb/
