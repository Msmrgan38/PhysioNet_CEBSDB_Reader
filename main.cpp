#include <iostream>
#include <fstream>
#include <vector>

#include "wfdb.h"

using namespace std;

std::vector<std::pair<double, double>> readECG(char *record, unsigned int channel)
{
    // WFDB have internal problem with database path, which defined before its building
    // Current database path: /home/user/wfdb-10.5.24/build/database/
    double sf = 1.0 / 5000.0; //sampling time (1/samplign frequency)
    WFDB_Sample v[4];
    WFDB_Siginfo s[4];
    if (isigopen(record, s, 4) < 1)
    {
        exit(1);
    }
    int nsamp = s[channel].nsamp;
    std::vector<std::pair<double, double>> res;
    for (int i = 0; i < nsamp; ++i)
    {
        if (getvec(v) < 0)
        {
            break;
        }
        res.push_back(std::make_pair(i * sf, v[channel]));
    }
    return res;
}

void writeCSV(std::vector<std::pair<double, double>> values, const char *filename="ecg.csv")
{
    ofstream csv_file(filename);
    for (auto it = values.cbegin(); it != values.cend(); ++it)
    {
        csv_file << (*it).first << "," << (*it).second << "\n";
    }
    csv_file.close();
}

int main(int argc, char *argv[])
{
    cout << "CEBS database Reading: b001 presumed healthy volunteer." << endl;

    std::vector<std::pair<double, double>> res = readECG("b001", 1);
    cout << "Saving ECG into CSV file..." << endl;
    writeCSV(res);
    cout << "Done." << endl;
    return 0;
}
